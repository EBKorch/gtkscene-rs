use cairo::Context;
use object::PixelObject;

use crate::object;

#[derive(Clone)]
pub enum Appearance {
    Agent,
    Item,
    Custom(fn(&Context, &PixelObject, &f64) -> ()),
}