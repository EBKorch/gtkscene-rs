use std::f64::consts::PI;

use units::{
    metric::{ Meters, Point },
    pixel::PixelPoint,
    traits::InPixels,
};

use super::appearance::Appearance;

#[derive(Clone)]
pub struct Object {
    pub pos: Point,
    pub rot: f64,
    pub appear: Appearance,
}

impl Object {
    pub fn new(appear: Appearance) -> Self {
        Object {
            appear,
            pos: Point::new(Meters(0.0), Meters(0.0)),
            rot: 0.0,
        }
    }

    pub fn from_raw(appear: Appearance, pos: Point, rot: f64) -> Self {
        Object {
            appear,
            pos,
            rot,
        }
    }

    
    pub fn set_loc<T: Into<Meters>, V: Into<Meters>>(&mut self, x: T, y: V, rotation: f64) {
        self.pos = Point::new(x, y);
        self.rot = rotation;
    }

    pub fn draw(&self, cr: &cairo::Context, scale: &f64, offset: &PixelPoint) {
        let pixelobject = self.clone().in_pixels(scale, offset);
        match pixelobject.appear {
            Appearance::Agent => {
                cr.arc(
                    pixelobject.pos.x.0,
                    pixelobject.pos.y.0,
                    Meters(0.01).in_pixels(scale, offset).0,
                    pixelobject.rot-7.0*PI/8.0,
                    pixelobject.rot+7.0*PI/8.0,
                );
                cr.line_to(pixelobject.pos.x.0, pixelobject.pos.y.0);
                cr.fill();
            }
            Appearance::Item => {

            }
            Appearance::Custom(fnc) => {
                // Draw the custom function
                fnc(cr, &pixelobject, scale)
            }
        }
    }

    pub fn visible(&self, width: &f64, height: &f64, scale: &f64, offset: &PixelPoint) -> bool {
        true
    }
}

pub struct PixelObject {
    pub pos: PixelPoint,
    pub rot: f64,
    pub appear: Appearance,
}

impl InPixels<PixelObject> for Object {
    fn in_pixels(self, scale: &f64, offset: &PixelPoint) -> PixelObject {
        PixelObject {
            pos: self.pos.in_pixels(scale, offset),
            rot: self.rot,
            appear: self.appear,
        }
    }
}
