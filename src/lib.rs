#[macro_export]
macro_rules! get_object {
    ( $builder:tt, $name:tt ) => {
        $builder.get_object($name).expect(&format!("Cannot find '{}' in ui file)", $name))
    }
}

pub mod scene;
pub mod object;
pub mod appearance;

#[cfg(test)]
mod tests {
}
