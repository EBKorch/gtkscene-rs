use std::{
    rc::Rc,
    cell::RefCell,
    collections::HashMap,
    time::Instant,
};

use gtk;
use gtk::prelude::*;
use glib::clone;

use units::{
    metric::Meters,
    pixel::{ Pixels, PixelPoint },
    traits::{ InMeters, InPixels },
};

use super::{
    object::Object,
    appearance::Appearance,
};

const SCROLL_SCALE_RATE: f64 = 1.25;
const SMOOTH_SCROLL_SCALE: f64 = 0.25;
const CLICK_DURATION_MS: u128 = 125;

pub struct Scene {
    pub objects: Rc<RefCell<HashMap<String, Object>>>,
    scale: Rc<RefCell<f64>>,
    offset: Rc<RefCell<PixelPoint>>,
    pub area: gtk::DrawingArea,
    pub transform_fn: Rc<RefCell<Box<dyn Fn(f64, &PixelPoint) -> ()>>>,
    pub item_click_fn: Rc<RefCell<Box<dyn Fn(&str) -> ()>>>,
}

impl Scene {
    pub fn new(area: gtk::DrawingArea) -> Self {
        let transform_fn: Rc<RefCell<Box<dyn Fn(f64, &PixelPoint) -> ()>>> = Rc::new(RefCell::new(Box::new(
            |_: f64,_: &PixelPoint| { }
        )));
        let item_click_fn: Rc<RefCell<Box<dyn Fn(&str) -> ()>>> = Rc::new(RefCell::new(Box::new(
            |_: &str| { }
        )));
        let scene = Scene {
            objects: Rc::new(RefCell::new(HashMap::new())),
            scale: Rc::new(RefCell::new(500.0)),
            offset: Rc::new(RefCell::new(PixelPoint { x: Pixels(0.0), y: Pixels(0.0) })),
            area,
            transform_fn,
            item_click_fn,
        };
        scene.connect();
        scene
    }

    pub fn set_item_click_fn<T: Fn(&str) -> () + 'static>(&self, fnc: T) {
        *self.item_click_fn.borrow_mut() = Box::new(fnc);
    }

    pub fn clear_objects(&self) -> HashMap<String, Object> {
        let objects;
        {
            objects = self.objects.borrow().to_owned();
        }
        self.objects.borrow_mut().clear();
        objects
    }

    pub fn replace_object<O: Into<Object>>(&self, name: &str, candidate: O) {
        self.objects.borrow_mut().insert(name.to_owned(), candidate.into());
    }

    pub fn set_transform_fn<T: Fn(f64, &PixelPoint) -> () + 'static>(&mut self, fnc: T) {
        *self.transform_fn.borrow_mut() = Box::new(fnc);
        (self.transform_fn.borrow())(*self.scale.borrow(), &*self.offset.borrow());
    }

    pub fn set_scale(&self, scale: f64) {
        if *self.scale.borrow() != scale {
            *self.scale.borrow_mut() = scale;
            self.update();
        }
    }

    pub fn set_offset(&self, offset_x: f64, offset_y: f64) {
        if self.offset.borrow().x.0 != offset_x || self.offset.borrow().y.0 != offset_y {
            *self.offset.borrow_mut() = PixelPoint{ x: Pixels(offset_x), y: Pixels(offset_y) };
            self.update();
        }
    }

    pub fn insert(&self, name: &str, object: Object) {
        self.objects.borrow_mut().insert(name.to_owned(), object);
        self.update();
    }

    pub fn insert_with_props(&self, name: &str, appear: Appearance, x: Meters, y: Meters, rot: f64) {
        let mut object = Object::new(appear);
        object.set_loc(x, y, rot);
        self.insert(name, object)
    }

    pub fn update(&self) {
        self.area.queue_draw_area(
            0, 0,
            self.area.get_allocated_width(), self.area.get_allocated_height()
        )
    }

    fn connect(&self) {
        // Fields from the instance
        let scale = Rc::clone(&self.scale);
        let offset = Rc::clone(&self.offset);

        // Enble events for the drawing area
        use gdk::EventMask;
        self.area.add_events(
            EventMask::SCROLL_MASK |
            EventMask::SMOOTH_SCROLL_MASK |
            EventMask::BUTTON_PRESS_MASK |
            EventMask::BUTTON_RELEASE_MASK |
            gdk::EventMask::POINTER_MOTION_MASK
        );

        // Scroll changes the scale of the scene
        let transform_fn = self.transform_fn.clone();
        self.area.connect_scroll_event(clone!(
            @strong scale, @strong offset, @strong transform_fn => move |w, scroll| {
                // Current values
                let current_scale = scale.borrow().clone();
                let current_offset = offset.borrow().clone();
                let delta = match scroll.get_direction() {
                    gdk::ScrollDirection::Up => current_scale*(SCROLL_SCALE_RATE-1.0),
                    gdk::ScrollDirection::Down => (current_scale/SCROLL_SCALE_RATE)-current_scale,
                    gdk::ScrollDirection::Smooth => current_scale*scroll.get_scroll_deltas().unwrap().1*SMOOTH_SCROLL_SCALE,
                    _ => return Inhibit(true),
                };
                // New values
                let new_scale = current_scale + delta;
                if new_scale <= 0.0 {
                    return Inhibit(false)
                }
                // Resolve offset
                let cursor = scroll.get_position();
                let cursor = PixelPoint { x: Pixels(cursor.0), y: Pixels(cursor.1) };
                let cursor_meters = cursor.clone().in_meters(&current_scale, &current_offset);
                let cursor_pixels = cursor_meters.in_pixels(&new_scale, &current_offset);
                let offset_delta = cursor - cursor_pixels.clone();

                // Set new values
                *scale.borrow_mut() = current_scale + delta;
                *offset.borrow_mut() = current_offset + offset_delta;

                // Run the transform function
                (transform_fn.borrow())(*scale.borrow(), &*offset.borrow());

                // Update the drawing area
                w.queue_draw_area(0, 0, w.get_allocated_width(), w.get_allocated_height());

                Inhibit(true)
            }
        ));

        // Grabbing the view changes the offset
        let clicktime = Rc::new(RefCell::new(Instant::now()));
        let startpos: Rc<RefCell<Option<PixelPoint>>> = Rc::new(RefCell::new(None));
        let startoffset = Rc::new(RefCell::new(self.offset.borrow().clone()));
        self.area.connect_button_press_event(clone!(
            @strong startpos, @strong startoffset, @strong offset, @strong clicktime => move |_, button| {
                // Measure the click time
                *clicktime.borrow_mut() = Instant::now();

                let current_startpos = startpos.borrow().clone();
                match current_startpos {
                    Some(_) => (),
                    None => {
                        let position = button.get_position();
                        *startpos.borrow_mut() = Some(PixelPoint { x: Pixels(position.0), y: Pixels(position.1) });
                        *startoffset.borrow_mut() = offset.borrow().clone();
                    }
                }

                Inhibit(false)
            }
        ));

        // Follow the mouse when the button is pushed
        self.area.connect_motion_notify_event(clone!(
            @strong startpos, @strong startoffset, @strong offset, @strong scale, @strong transform_fn => move |w, event| {
                let startpos = &*startpos.borrow();
                match startpos {
                    Some(position) => {
                        let new_position = event.get_position();
                        let new_position = PixelPoint{ x: Pixels(new_position.0), y: Pixels(new_position.1) };
                        *offset.borrow_mut() = startoffset.borrow().clone() + (new_position - position.clone());
                        // We also need to update the view
                        w.queue_draw_area(0, 0, w.get_allocated_width(), w.get_allocated_height());
                        // Run the transform function
                        (transform_fn.borrow())(*scale.borrow(), &*offset.borrow());
                        // Do not propagate the event
                        Inhibit(false)
                    }
                    None => Inhibit(true),
                }
            }
        ));

        // Stop the grab when the drawing area is release
        let objects = Rc::clone(&self.objects);
        let item_click_fn = self.item_click_fn.clone();
        self.area.connect_button_release_event(clone!(
            @strong scale, @strong offset, @strong objects => move |_, button| {

            *startpos.borrow_mut() = None;
            if clicktime.borrow().elapsed().as_millis() < CLICK_DURATION_MS {
                // Get the mouse position
                let (mousex, mousey) = button.get_coords().unwrap();
                let mouse = PixelPoint { x: Pixels(mousex), y: Pixels(mousey) };
                // Convert it from pixel coordinate to physical position
                let mouse = mouse.in_meters(&scale.borrow(), &*offset.borrow());
                // Find the closest object
                let mut min_distance = std::f64::INFINITY;
                let mut min_name = String::from("");
                for (name, object) in objects.borrow().iter() {
                    let (dx, dy) = mouse.clone() - object.pos.clone();
                    let distance = dx.0.powi(2) + dy.0.powi(2);
                    if distance < min_distance {
                        min_distance = distance;
                        min_name = name.clone();
                    }
                }
                // Select the item (if any found) if it is close enough to the
                // cursor
                if min_distance < std::f64::INFINITY &&
                  (min_distance < 0.001 || Meters(min_distance).in_pixels(&scale.borrow(), &offset.borrow()).0 < 5.0) {
                    (*item_click_fn.borrow())(&min_name);
                }
            }
            Inhibit(false)
        }));

        // The drawing event
        self.area.connect_draw(move |widget, cr| {
            let ctx = widget.get_style_context();
            let foreground_color = ctx.get_color(ctx.get_state());
            let width = widget.get_allocated_width() as f64;
            let height = widget.get_allocated_height() as f64;

            gtk::render_background(&ctx, cr, 0.0, 0.0, width, height);
            cr.set_line_width(2.0);
            cr.set_source_rgba(foreground_color.red, foreground_color.green, foreground_color.blue, foreground_color.alpha);

            for (_, object) in objects.borrow().iter() {
                if object.visible(&width, &height, &scale.borrow(), &offset.borrow()) {
                    object.draw(cr, &scale.borrow(), &offset.borrow());
                }
            }

            Inhibit(false)
        });
    }
}
